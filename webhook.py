import requests
from flask import Flask, request, abort

API_URL = "https://panel.apiwha.com/send_message.php"
API_KEY = "api_key"
DESTINATION_NUMBER = "5511986924400"

app = Flask(__name__)

grafana_msg = {
  	"evalMatches":
    {
      	"value": 100,
      	"metric": "High value",
      	"tags": "Geladeira 1"
    },
  	"message": "Someone is testing the alert notification within grafana.",
  	"ruleId": 0,
  	"ruleName": "Test notification",
  	"ruleUrl": "http://localhost:3000/",
  	"state": "alerting",
  	"title": "[Alerting] Test notification"
}


def _parse_grafana_msg(message):
	data = {}
	if message:
		data["tag"] = message["evalMatches"]["tags"]
		data["temperature"] = message["evalMatches"]["value"]
		data["expected"] = message["evalMatches"]["metric"]
		return data
	return None

def _build_whatsapp_message(data):
	if data:
		tag = data["tag"]
		temperature = data["temperature"]
		expected = data["expected"]
		message = "DEV Sense Alerta: O sensor {tag} está medindo {temperature} ao invés do esperado {expected}".format(tag=tag, temperature=temperature, expected=expected)
		return message
	return None


def _send_whatsapp_message(message):
	querystring = {"apikey":API_KEY,"number":DESTINATION_NUMBER,"text":message}
	response = requests.request("GET", API_URL, params=querystring)
	return response.text


def _return_whatsapp_message(data):
	output = _parse_grafana_msg(data)
	output = _build_whatsapp_message(output)
	# output = _send_whatsapp_message(output)
	return output


@app.route('/webhook', methods=['POST'])
def webhook():
    if request.method == 'POST':
        print(request.json)
        output = _return_whatsapp_message(request.json)
        print(output)
        return '', 200
    else:
        abort(400)


if __name__ == '__main__':
    app.run()
