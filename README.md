# webhook with grafana

# prerequisites

sudo apt install python3-pip

pip3 install flask

# let's get started

start listener:

python3 webhook.py

send message:

curl -H "Content-Type: application/json" -X POST -d '{"evalMatches":{"value":100,"metric":"High value","tags":"Geladeira 1"},"message":"Someone is testing the alert notification within grafana.","ruleId":0,"ruleName":"Test notification","ruleUrl":"http://localhost:3000/","state":"alerting","title":"[Alerting] Test notification"}' 127.0.0.1:5000/webhook

expected response:

DEV Sense Alerta: O sensor Geladeira 1 está medindo 100 ao invés do esperado High value
